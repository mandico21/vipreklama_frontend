import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/pages/MainPage'),
    meta: {
      title: 'Главная',
      layout: 'MainLayout',
    },
  },
  {
    path: '/category/:category',
    name: 'categoty',
    component: () => import('@/pages/CategoryPage.vue'),
    meta: {
      layout: 'MainLayout',
    },
  },
  {
    path: '/news',
    name: 'news',
    component: () => import('@/pages/NewsPage'),
    meta: {
      layout: 'MainLayout',
    },
  },
  {
    path: '/instructions',
    name: 'instructions',
    component: () => import('@/pages/InstructionsPage'),
    meta: {
      title: 'Инструкция',
      layout: 'DefaultLayout',
    },
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('@/pages/AboutPage'),
    meta: {
      title: 'О нас',
      layout: 'DefaultLayout',
    },
  },
  {
    path: '/reklama',
    name: 'reklama',
    component: () => import('@/pages/AdvertisementPage'),
    meta: {
      title: 'Реклама',
      layout: 'DefaultLayout',
    },
  },
  {
    path: '/ads/:id',
    name: 'ads',
    component: () => import('@/pages/OffersPage'),
    meta: {
      layout: 'DefaultLayout',
    },
  },
  {
    path: '/account',
    name: 'account',
    component: () => import('@/pages/AccountPage'),
    meta: {
      title: 'Личный кабинет',
      layout: 'DefaultLayout',
    },
  },
  {
    path: '/add',
    name: 'add',
    component: () => import('@/pages/AddAd'),
    meta: {
      title: 'Добавить объявление',
      layout: 'DefaultLayout',
    },
  },
]

const router = createRouter({
  routes,
  history: createWebHistory(process.env.BASE_URL),
  scrollBehavior() {
    return { top: 0 }
  },
})

export default router

import { createApp } from 'vue'
import App from './App'
import { Vuelidate } from 'vuelidate'
import components from '@/components/UI'
import router from '@/router/router'
import store from '@/store'
import axios from 'axios'

const app = createApp(App)

components.forEach((component) => {
  app.component(component.name, component)
})

axios.defaults.baseURL = 'http://127.0.0.1:8000/api/v1'
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')
axios.defaults.headers.post['Content-Type'] =
  'application/x-www-form-urlencoded'

app.use(router).use(store).use(Vuelidate).mount('#app')

import axios from 'axios'

export const page = {
  state: () => ({
    news: [],
    tags: [],
    slider: [],
    banners: [],
    categories: [],
  }),
  getters: {},
  mutations: {
    setPage(state, paload) {
      state.news = paload.news
      state.tags = paload.tags
      state.slider = paload.slider
      state.banners = paload.banners
      state.categories = paload.categories
    },
  },
  actions: {
    async fetchPage({ commit }) {
      try {
        const response = await axios.get(
          'http://127.0.0.1:8000/api/v1/get_page'
        )
        console.log(response)
        commit('setPage', response.data)
      } catch (e) {
        console.log(e)
      }
    },
  },
  modules: {},
  namespaced: true,
}

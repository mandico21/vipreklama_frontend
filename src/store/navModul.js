export const navModul = {
  state: () => ({
    isActive: false,
  }),
  getters: {},
  mutations: {
    openClosing(state) {
      if (state.isActive) {
        return (state.isActive = false)
      } else {
        return (state.isActive = true)
      }
    },
  },
  actions: {},
  modules: {},
  namespaced: true,
}

import axios from 'axios'

export const auth = {
  state: () => ({
    credentials: {
      token: localStorage.getItem('token') || null,
      isActive: false,
    },
    user: {
      telegram_id: 0,
      username: '',
    },
  }),
  getters: {},
  mutations: {
    setActive(state, pyload) {
      if (pyload.success === true) {
        state.credentials.isActive = pyload.active
        state.user = pyload.user
        localStorage.setItem('token', pyload.token)
      }
    },
  },
  actions: {
    async fetchAuth({ commit }, telegram_id) {
      try {
        const response = await axios.post(
          'http://127.0.0.1:8000/api/m2m/auth' + telegram_id
        )
        commit('setActive', response.data)
      } catch (e) {
        console.log(e)
      }
    },
    async fetchAuthCheck({ commit }) {
      try {
        const response = await axios.post(
          'http://127.0.0.1:8000/api/m2m/auth/check'
        )
        commit('setActive', response.data)
      } catch (e) {
        console.log(e)
      }
    },
  },
  modules: {},
  namespaced: true,
}

import { createStore } from 'vuex'
import { page } from '@/store/page'
import { auth } from '@/store/auth'
import { navModul } from '@/store/navModul'

export default createStore({
  modules: {
    page: page,
    auth: auth,
    nav: navModul,
  },
})

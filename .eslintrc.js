module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: '@babel/eslint-parser'
  },
  extends: ['plugin:vue/vue3-essential', 'eslint:recommended', 'eslint:recommended'],
  plugins: [],
  // add your custom rules here
  rules: {
    'space-before-function-paren': 0,
    camelcase: 'off',
    'no-console': 'off',
    'array-callback-return': 0,
    'no-unused-vars': 1,
    'arrow-parens': 0,
    'vue/html-self-closing': 0,
    'no-irregular-whitespace': 0,
    indent: 'off',
    'vue/no-v-html': 0,
    'vue/order-in-components': 0,
    'import/order': 0
  }
}
